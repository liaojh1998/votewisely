import mysql.connector
from mysql.connector import errorcode
import requests
import xlrd

config = {
    "user": "votewisely",
    "password": "votewisely123",
    "host": "cs373votewisely.cqqkkpkbowt8.us-east-1.rds.amazonaws.com",
    "port": "3306",
    "database": "VoteWiselyDB",
    "raise_on_warnings": True,
}


def get_request():
    """
    Make GET API call to the URL using API_KEY
    Get the abbrev, name, legislature_name
    :return:
    """

    url = f"https://openstates.org/api/v1/metadata?apikey=0d28c2ca-fd46-4c06-a241-cec8da7b08a0"

    res = requests.get(url=url)
    if res.status_code != 200:
        raise Exception(f"GET /tasks/ {res.status_code}")
    else:
        print("API GET request successful!")
    return res.json()


def create_table(table):
    table_description = (
        "  CREATE TABLE IF NOT EXISTS `states_table` ("
        "   `abbrev` char(10) NOT NULL,"
        "   `name` char(30) NOT NULL,"
        "   `population` INT NOT NULL,"
        "   `banner_url` char(128) NOT NULL,"
        "   `legislature_name` char(128) NOT NULL"
        ") ENGINE=InnoDB"
        "   CREATE UNIQUE INDEX states_table_abbrev_uindex"
        "       on states_table (abbrev)"
        "   ALTER TABLE states_table"
        "       ADD CONSTRAINT states_table_pk"
    )
    try:
        print(f"Creating table {table}: ")
        cursor.execute(table_description)
    except mysql.connector.Error as e:
        if e.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(e.msg)
    else:
        print("OK")


def insert_state_data(states):
    table = "states_table"
    attributes = ("abbrev", "name", "population", "banner_url", "legislature_name")
    add_state = (
        f"INSERT INTO {table} "
        "(abbrev, name, population, banner_url, legislature_name) "
        "VALUES (%(abbrev)s, %(name)s, %(population)s, %(banner_url)s, %(legislature_name)s)"
    )
    # read the population into a dictionary
    loc = "./Book1.xlsx"
    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    population_dict = {}
    for r in range(sheet.nrows):
        population_dict[sheet.cell_value(r, 0)[1:]] = sheet.cell_value(r, 1)
    if len(population_dict.keys()) != 50:
        raise Exception("Reading from sheet error: not enough state read!")
    # build and execute the insert state statements
    for state in states:
        if state["id"] != "pr" and state["id"] != "dc":
            state_data = {}
            state_abbrev = state["id"].upper()
            state_data["abbrev"] = state_abbrev
            state_data["name"] = state["name"]
            state_data["population"] = population_dict[state["name"]]
            state_data["banner_url"] = (
                "https://www.50states.com/images/redesign/flags/"
                + state_abbrev
                + "-largeflag.png"
            )
            state_data["legislature_name"] = state["legislature_name"]
            cursor.execute(add_state, state_data)

    return 0  # success


try:
    cnx = mysql.connector.connect(**config)
    print("Connection successful!")
    cursor = cnx.cursor()

    if __name__ == "__main__":
        r = get_request()

        # print(f"Num results queried:{results['num_results']}")

        # table_name = "states_table"
        # create_table(table_name)
        insert_state_data(r)
        cnx.commit()  # only when you insert or update table


except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)
else:
    cursor.close()
    cnx.close()
