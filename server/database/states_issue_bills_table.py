import json
import requests

from apikey import apikey
import demjson
from bills_table_2 import get_request
import issues_table_2 as it
import table_api as sql

table_name = "states_issue_bills"
table_schema = {
    "state": ("CHAR(20)", "NOT NULL"),
    "issue": ("TEXT", "NOT NULL"),
    "count": ("INT", "NOT NULL", "DEFAULT 0"),
}
foreign_keys = {"state": "states_table(abbrev)"}


def get_all_bills(issue_topic):
    offset = 0
    issue_bills = []
    res = get_request(issue_topic, offset)
    while len(res) > 0 and offset < 1000:
        issue_bills += res
        offset += 20
        if offset < 1000:
            res = get_request(issue_topic, offset)
    return issue_bills


def insert_bills_count_by_state():
    issues = sql.sql_fetch_all(f"SELECT {it.primary_key}, name FROM {it.table_name}")
    for issue, name in issues:
        bills = get_all_bills(name)
        bills_count_by_state = {}
        counter = 0
        for bill in bills:
            has_state = bill["sponsor_state"] in bills_count_by_state
            if not has_state:
                has_state = sql.sql_fetch_all(
                    f"SELECT abbrev FROM states_table WHERE abbrev=\"{bill['sponsor_state']}\""
                )
            if has_state:
                state = bill["sponsor_state"]
                if not state in bills_count_by_state:
                    bills_count_by_state[state] = {}
                bills_count_by_state[state][name] = (
                    bills_count_by_state[state].get(name, 0) + 1
                )
                counter += 1
        print(f"Accounted for {counter} out of {len(bills)} bills in issue {name}")
        for state, stats in bills_count_by_state.items():
            for issue, count in stats.items():
                sql.insert_table(
                    table_name,
                    table_schema,
                    {"state": state, "issue": issue, "count": count},
                )


if __name__ == "__main__":
    sql.create_table(table_name, table_schema, foreign_keys)
    insert_bills_count_by_state()
