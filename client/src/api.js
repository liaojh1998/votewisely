var nodelink = "";

if (process.env.NODE_ENV === "development") {
    nodelink = "http://localhost:3001";
} else if (process.env.REACT_APP_STAGING) {
    nodelink = "https://thewisevote.com/staged";
}

module.exports = { nodelink };
