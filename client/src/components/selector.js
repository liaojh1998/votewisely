import React, { PureComponent } from 'react';

export default class Selector extends PureComponent {
  constructor(props) {
    /* Props args: list, onClick, defaultIndex (optional) */
    super(props);
    this.state = {
      index: this.props.defaultIndex
    };
  }

  async changeIndex(idx) {
    await this.setState({
      index: idx
    });
    var name = this.props.list[idx];
    this.props.onClick(name);
  }

  render() {
    return (
      <div className="selector">
        {
          this.props.list.map((name, idx) =>
            <button
                key={idx}
                className={this.state.index === idx ? "btn btn-primary" : "btn"}
                onClick={() => this.changeIndex(idx)}>
              {name}
            </button>
          )
        }
      </div>
    );
  }
}
