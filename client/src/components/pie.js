import React, {PureComponent} from 'react';
import * as d3 from "d3";
import Slice from './slice';
import { nodelink } from '../api';
import { DropdownButton } from "react-bootstrap";
import DropdownFilter from '../components/dropdownFilter';

export default class Pie extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      stateFilter: 'AK',
    };
    this.handleStateFilter = this.handleStateFilter.bind(this);
  }

  componentDidMount() {
    fetch(nodelink + "/api/catsperstate")
      .then(res => res.json())
      .then(res => res.response)
      .then(states => {
      var statesList = [];

      for (var j = 0; j < states.length; j++) {
        statesList.push(states[j])
      }

      this.setState({
        statesList: statesList
      });
    });

    fetch(nodelink + "/api/catsperstate/" + this.state.stateFilter)
      .then(res => res.json())
      .then(res => res.response)
      .then(categories => {
        var categoryList = [];
        var data = [];

        var totalCount = 0;
        for (var k = 0; k < categories.length; k++) {
            totalCount += categories[k].count;
        }

        var minPercent = totalCount * .04;

        for (var i = 0; i < categories.length; i++) {
          if (categories[i].count >= minPercent) {
            categoryList.push(categories[i].category)
            data.push(categories[i].count)
          }
        }

        this.setState({
          categoryList : categoryList,
          data : data
        });
      });
  }

  handleStateFilter(s) {
    fetch(nodelink + "/api/catsperstate/" + s)
      .then(res => res.json())
      .then(res => res.response)
      .then(categories => {
        var categoryList = [];
        var data = [];

        var totalCount = 0;
        for (var k = 0; k < categories.length; k++) {
            totalCount += categories[k].count;
        }

        var minPercent = totalCount * .04;

        for (var i = 0; i < categories.length; i++) {
          if (categories[i].count >= minPercent) {
            categoryList.push(categories[i].category)
            data.push(categories[i].count)
          }
        }

        this.setState({
          categoryList : categoryList,
          data : data,
          stateFilter: s
        });
      });
  }

  renderStateDropdown() {
    if (this.state.statesList) {
      return (
        <DropdownButton id="dropdown-basic-button" title={"Pick a state: " + this.state.stateFilter}>
          <DropdownFilter
            list={this.state.statesList}
            onClick={this.handleStateFilter}
            defaultIndex={0} />
        </DropdownButton>
      );
    }
  }

  componentDidUpdate() {
    if (this.state.data) {
      const svg = d3.select("#pie");
      const dimensions = svg.node().getBoundingClientRect();
      svg.attr("viewBox",
        `${-dimensions.width / 2} ${-dimensions.height / 2} ${dimensions.width} ${dimensions.height}`);
    }
  }

  render() {
    if (this.state.data) {
      var data = this.state.data;
      var arcs = d3.pie()(data);
      var innerRadius = 0;
      var outerRadius = 250;
      return (
        <div className='box' style={{background: "whitesmoke", paddingBottom: '40px', borderRadius: '10px', marginTop: '50px'}}>
          <h3>Number of Bills in each Issue Category by State</h3>
          {this.renderStateDropdown()}
          <svg width="calc(100% - 20)" height="500" id="pie" style={{"paddingTop": '10px'}}>
            {arcs.map((obj, i) =>
              <Slice
                key={"slice" + i}
                innerRadius = {innerRadius}
                outerRadius = {outerRadius}
                startAngle = {obj.startAngle}
                endAngle = {obj.endAngle}
                fillColor = {d3.rgb(Math.random() * (180 - 1) + 1,
                             Math.random() * (180 - 1) + 1, Math.random() * (180 - 60) + 1)}
              />
            )}
            {arcs.map((obj, i) => 
              <text key={"label" + i}
                    className="slice-text"
                    transform={`translate(${d3.arc()
                      .centroid({
                        innerRadius: outerRadius / 2,
                        outerRadius: outerRadius,
                        startAngle: obj.startAngle,
                        endAngle: obj.endAngle})})`
                    }
                    textAnchor="middle"
                    fill="white"
                    style={{fontSize: 13}}>
                {this.state.categoryList[i]}
                <tspan style={{fontSize: 10}}>
                  {" " + obj.data + " Bills"}
                </tspan>
              </text>
            )}
          </svg>
        </div>
      );
    }
    return ('Loading')
  }
}
