import React, { PureComponent } from 'react';
import * as d3 from "d3";

import { nodelink } from '../api';
import '../css/repsBubbleChart.css';

export default class RepsBubbleChart extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    fetch(nodelink + '/api/issuesperrep')
      .then(res => res.json())
      .then(res => res.response)
      .then(res => this.setState({ data: res }))
  }

  componentDidUpdate() {
    if (this.state.data) {
      // Dimensions
      const sliderWidth = 40;
      const width = `calc(100% - ${sliderWidth}px * 2)`;
      const height = 550;
      const scaleLimit = 75;

      // Get svg DOM
      const svg = d3.select("#repsBubbleChart")
        .style("width", width)
        .style("height", height);

      // Create circles
      const dimensions = svg.node().getBoundingClientRect();
      const hierarchy = d3.hierarchy({ children: this.state.data })
        .sum(d => d.issues)
        .sort((a, b) => b.value - a.value);
      const pack = d3.pack()
        .size([dimensions.width, dimensions.height])
        .padding(2);
      const circlesProperties = pack(hierarchy);

      // Circles group configuration
      const circlesGroup = svg.append("g")
        .attr("id", "circles")
        .attr("transform", "translate(0,0) scale(1)");

      // Setup group for each circle
      const circles = circlesGroup.selectAll("g")
        .data(circlesProperties.leaves())
        .join("g");

      // Add the circles
      circles.append("circle")
        .attr("cy", d => d.y)
        .attr("cx", d => d.x)
        .attr("r", d => d.r)
        .attr("fill", d => d.data.party === "R" ? "red" : (d.data.party === "D" ? "blue" : "green"));

      // Update labels
      function updateLabels(transform, minRadius) {
        if (transform.k > minRadius) {
          circles.each(function(d) {
            const circle = d3.select(this);
            const currentDimensions = svg.node().getBoundingClientRect();
            if (Math.abs(transform.k * d.x + transform.x - currentDimensions.width / 2) < currentDimensions.width / 2 + transform.k * d.r
                && Math.abs(transform.k * d.y + transform.y - currentDimensions.height / 2) < currentDimensions.height / 2 + transform.k * d.r) {
              if (circle.select("text").empty()) {
                // Add label to the circle
                const label = circle.append("text")
                  .attr("x", d => d.x)
                  .attr("y", d => d.y)
                  .attr("text-anchor", "middle");

                // Add politician link to label
                label.append("a")
                  .attr("href", d => "/politicians/" + d.data.rep_name.toLowerCase().replace(/\s+/g, '-'))
                  .style("font-size", d => d.r / d.data.rep_name.length * 3)
                  .style("fill", d => d.data.party !== "R" ? "white" : "black")
                  .text(d => d.data.rep_name);
                
                // Add issues covered by politician
                label.append("tspan")
                  .attr("x", d => d.x)
                  .attr("y", d => d.y + d.r / d.data.rep_name.length * 2.5)
                  .style("font-size", d => d.r / d.data.rep_name.length * 1.5)
                  .style("fill", d => d.data.party !== "R" ? "white" : "black")
                  .text(d => "Issues covered: " + d.data.issues);
              }
            } else {
              circle.select("text").remove();
            }
          })
        } else {
          circles.select("text").remove();
        }
      }

      // Dragging for the circles
      const circlesDragBehavior = d3.drag().on("start", () => {
        d3.event.on("drag", () => {
          const s = circlesGroup.node().transform.baseVal.consolidate().matrix.a;
          circlesZoomBehavior.translateBy(svg, d3.event.dx / s, d3.event.dy / s);
        });
      });
      svg.call(circlesDragBehavior)

      // Zooming for the circles
      const circlesZoomBehavior = d3.zoom().on("zoom", () => {
        const group = d3.select("#circles");
        const transform = d3.event.transform;
        group.attr("transform", "translate(" + transform.x + ","
          + transform.y + ") scale(" + transform.k + ")");
        button.attr("transform", "translate(0," + scale.invert(transform.k) + ")");
        updateLabels(transform, 6.5);
      })
      .filter(() => d3.event.type === "wheel")
      .scaleExtent([1, scaleLimit]);
      svg.call(circlesZoomBehavior)
        .on("wheel", () => { d3.event.preventDefault(); });

      // Draw the border for the SVG
      svg.append("rect")
        .attr("width", `calc(${sliderWidth}px * 2 + ${width})`)
        .attr("height", height)
        .attr("fill", "none")
        .attr("stroke", "black")
        .attr("stroke-width", 1);

      // Get slider DOM
      const slider = d3.select("#slider")
        .style("width", sliderWidth)
        .style("height", height);

      // Draw the black slider outline
      const track = slider.append("line")
        .attr("x1", sliderWidth / 3 - 2)
        .attr("x2", sliderWidth / 3 - 2)
        .attr("y1", 10)
        .attr("y2", height - 10)
        .attr("stroke", "black")
        .attr("stroke-width", "10px")
        .attr("stroke-linecap", "round");

      // Add a light gray inside
      track.select(function () { return this.parentNode.appendChild(this.cloneNode(true)); })
        .attr("stroke", "silver")
        .attr("stroke-width", "8px");

      // Label slider
      slider.append("text")
        .attr("x", -height / 2)
        .attr("y", sliderWidth - 7)
        .attr("transform", "rotate(-90)")
        .attr("text-anchor", "middle")
        .text("Zoom Scale")
        .style("font-size", 13);
      slider.append("text")
        .attr("x", -30)
        .attr("y", sliderWidth - 7)
        .attr("transform", "rotate(-90)")
        .attr("text-anchor", "middle")
        .text("Biggest")
        .style("font-size", 13);
      slider.append("text")
        .attr("x", -height + 30)
        .attr("y", sliderWidth - 7)
        .attr("transform", "rotate(-90)")
        .attr("text-anchor", "middle")
        .text("Smallest")
        .style("font-size", 13);

      // Create a sliding button
      var button = slider.insert("circle")
        .attr("id", "sliderButton")
        .attr("cx", sliderWidth / 3 - 2)
        .attr("cy", height - 10)
        .attr("r", 7.5)
        .attr("transform", "translate(0,0)")
        .attr("stroke", "black")
        .attr("stroke-width", 1)
        .attr("fill", "white");

      // Create a scale for the slider
      var scale = d3.scaleLinear()
        .domain([0, 20 - height])
        .range([1, scaleLimit]);

      // Button drag to zoom behavior
      var buttonDragBehavior = d3.drag().on("start", () => {
        d3.event.on("drag", () => {
          const y = button.node().transform.baseVal.consolidate().matrix.f + d3.event.dy;
          if (y <= 0 && y >= 20 - height)
            circlesZoomBehavior.scaleTo(svg, scale(y));
        });
      });
      button.call(buttonDragBehavior);
    }
  }

  render() {
    if (this.state.data) {
      return (
        <div style={{background: "whitesmoke", paddingBottom: '40px', borderRadius: '10px', marginTop: '50px'}}>
            <br />
            <h5>Number of Issues Covered by the Bills of Each Politician</h5>
            <svg id="repsBubbleChart" />
            <svg id="slider" />
        </div>
      );
    }
    return (
      <div style={{background: "whitesmoke", paddingBottom: '40px', borderRadius: '10px', marginTop: '50px'}}>
        <br />
        <h5>Number of Issues Covered by the Bills of Each Politician</h5>
        <h3>Loading...</h3>
      </div>
    );
  }
}
