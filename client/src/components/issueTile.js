import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

export default class IssueTile extends PureComponent {
  highlight(words, highlighter) {
    /* Exact search takes precedences */
    var newWords = words;
    if (highlighter.exact && highlighter.exact.length > 0) {
      var exactRegex = RegExp(highlighter.exact
        .map(w => '(?=' + w + ')|(?<=' + w + ')').join('|'), "g");
      newWords = newWords.split(exactRegex);
      newWords = newWords.map((w, idx) =>
        highlighter.exact.includes(w) ?
        <span key={'e' + idx} style={{ "backgroundColor": "#ffff00" }}>{w}</span> : w);
    } else {
      newWords = [newWords];
    }
    /* Keywords search comes next */
    if (highlighter.keywords && highlighter.keywords.length > 0) {
      var keywordsRegex = RegExp(highlighter.keywords
        .map(w => '(?=' + w + ')|(?<=' + w + ')').join('|'), "gi");
      newWords = newWords.map((w, idx) =>
        typeof w === 'string' ? w.split(keywordsRegex) : [w])
        .reduce((acc, cur) => acc.concat(cur), []);
      newWords = newWords.map((w, idx) =>
        typeof w === 'string' && highlighter.keywords.includes(w.toLowerCase()) ?
        <span key={'w' + idx} style={{ "backgroundColor": "#ffff00" }}>{w}</span> : w);
    }
    return newWords;
  }

  renderName() {
    if (this.props.highlight) {
      return this.highlight(this.props.data.name, this.props.highlight);
    }
    return this.props.data.name;
  }

  renderSummary() {
    if (this.props.highlight) {
      return this.highlight(this.props.data.summary, this.props.highlight);
    }
    return this.props.data.summary;
  }

  renderLink(linkName) {
    return "/issues/" + linkName;
  }

  renderPercent(num) {
    return num + "%";
  }

  render() {
    return (
      <div id="card" className="issue-tile">
          <Link to={this.renderLink(this.props.data.route)}>
            <img src={this.props.data.icon} alt="" className="tile-image"/>
          </Link>
          <Link to={this.renderLink(this.props.data.route)}>
            <h2>{this.renderName()}</h2>
          </Link>
          <p><strong>Description: </strong>{this.renderSummary()}</p>
          <p><strong>Essential Question: </strong>{this.props.data.question}</p>
          <p><strong>Total Votes: </strong>{this.props.data.votes_total}</p>
          <p><strong>Votes for: </strong>{this.renderPercent(this.props.data.pct_yes)}</p>
          <p><strong>Votes against: </strong>{this.renderPercent(this.props.data.pct_no)}</p>
          <p><strong>Votes neutral: </strong>{this.renderPercent(this.props.data.pct_neutral)}</p>
          <p><strong>Category: </strong>{this.props.data.category}</p>

          <p>
            <Link to={this.renderLink(this.props.data.route)}>
              Learn More
            </Link>
          </p>
      </div>
    );
  }
}
