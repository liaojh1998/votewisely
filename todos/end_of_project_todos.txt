Add slides of front end, backend, and database structure

Low Priority:
Actually correctly send 404 status
Fix mui-datatables package warnings with rowsPerPageOptions as a number instead of an array
Change reps API to make it fast by passing less information at once
Dialog to bill, statements
Searching for instances with null fields in dababase will actually not show up
Fix States column name to have arrow on the side (change width of column)
Synchronize commits on the about pages
Unify issues color scheme, website profile page themes
ESLint to pretty code and add more documentations

People who do not have a description but has a name:

"Robert Brady": "Bob Brady",
"G. Butterfield": "G. K. Butterfield",
"David Brat": "Dave Brat",
"Donald Beyer": "Don Beyer",
"Steven Chabot": "Steve Chabot",
"James Clyburn": "Jim Clyburn",
"Michael Crapo": "Mike Crapo",
"Michael Capuano": "Mike Capuano",
"Shelley Capito": "Shelley Moore Capito",
"William Clay": "Lacy Clay",
"K. Conaway": "Mike Conaway",
"Bob Casey": "Bob Casey Jr.",
"Gerald Connolly": "Gerry Connolly",
"Christopher Coons": "Chris Coons",
"Jeffery Denham": "Jeff Denham",
"Robert Goodlatte": "Bob Goodlatte",
"Charles Grassley": "Chuck Grassley",

Start from Thomas Garrett for next search...